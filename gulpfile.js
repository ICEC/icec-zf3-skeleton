var gulp = require('gulp'),
    concat = require("./vendor/node_modules/lib/node_modules/gulp-concat"),
    sass = require('./vendor/node_modules/lib/node_modules/gulp-sass'),
    connect = require('./vendor/node_modules/lib/node_modules/gulp-connect'),
    minify = require('./vendor/node_modules/lib/node_modules/gulp-minify');

gulp.task('connect',function () {
    connect.server({
        root:'public',
        livereload:true
    });
});

gulp.task('sass',function () {
    return gulp.src([
        './sass/**/*.scss',
        './vendor/bower_components/fullcalendar/dist/fullcalendar.min.css',
        './vendor/bower_components/motion-ui/dist/motion-ui.min.css'
    ])
        .pipe(sass({errLogToConsole:true}))
        .pipe(gulp.dest('./public/css'))
});

// FountAwesome-icons
gulp.task('fontsawesome', function() {
    return gulp.src([
        './vendor/bower_components/fontawesome/fonts/fontawesome-webfont.*'])
        .pipe(gulp.dest('./public/fonts/'));
});

// Foundation-icons
    gulp.task ('copyfonts',function () {
        gulp.src ('./vendor/bower_components/foundation-icon-fonts/**/*.{ttf,woff,eoff,svg}')
            .pipe (gulp.dest('./public/css'));
    } ) ;

gulp.task('scripts',function () {
    return gulp.src([
        // Include jQuery
        './vendor/bower_components/jquery/dist/jquery.min.js',
        './vendor/bower_components/foundation-sites/dist/js/foundation.min.js',
        './vendor/bower_components/what-input/dist/what-input.min.js',
        
        './vendor/bower_components/chart.js/dist/Chart.bundle.min.js',
        './vendor/bower_components/moment/min/moment.min.js',
        './vendor/bower_components/fullcalendar/dist/fullcalendar.min.js',
        './vendor/bower_components/fullcalendar/dist/locale/nl.js',
        './vendor/bower_components/datatables.net/js/jquery.dataTables.min.js'

    ])
        //TODO fix minification of js
        // .pipe(minify({
        //     ext:{
        //         src:'-debug.js',
        //         min:'.js'
        //     },
        //     exclude: ['tasks'],
        //     ignoreFiles: ['.combo.js', '-min.js']
        // }))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('./public/js'))
});

gulp.task('livereload',function () {
    gulp.src('./public/**/*')
        .pipe(connect.reload());
});

gulp.task('watch',function () {
    gulp.watch('./sass/**/*.scss',['sass']);
    gulp.watch('./public/**/*',['livereload']);
});

gulp.task('default',['connect','watch','sass','scripts','copyfonts','fontsawesome']);


