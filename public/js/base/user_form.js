(function (window, document, $, undefined) {
    
    
    A2C.ValidateEmailInput = function(e) {
    
        // Add loader
        var $emailElement = $(this);
        var email_input = $emailElement.val();
        var user_id = $('input[type="hidden"][name="id"]').val();
        var $errorElement = $emailElement.parent().find('span[class*="form-error"]');
        $errorElement.removeClass('is-visible').html();
        $emailElement.removeClass('is-invalid-input');
        if (!validateEmail(email_input)) {
            var error_text = 'Ingevoerde email voldoet niet aan een emailadres formaat (gebruiker@2coolbv.com)';
            $emailElement.addClass('is-invalid-input');
            $errorElement.html(error_text).addClass('is-visible');
            e.preventDefault();
            return;
        }
        
        $.getJSON({
            dataType: "json",
            url: '/ajax/validateEmail',
            type: 'post',
            data: {
                email_input: email_input,
                user_id: user_id
            },
            success: function (data) {
                if(! data.show_as_valid){
                    $emailElement.addClass('is-invalid-input');
                    $errorElement.html(data.error_text).addClass('is-visible');
                }
            }
        });
    };
    
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
    
    
    $(function () {
        $(document)
            .on('change','input[type="email"]',A2C.ValidateEmailInput)
        ;
    });
    
}(window, document, jQuery));