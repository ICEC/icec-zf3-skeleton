(function (window, document, $, undefined) {
    var DoDraw = function () {
        // Setup canvas and context variables
        var $element = $('input[data-unique-name="canvasinput"]');
        var $canvas = $('canvas[data-unique-name="canvas"]');
        var $clear = $('i[data-unique-name="canvasclear"]');
        var canvas = $canvas.get(0);
        var context = canvas.getContext('2d');
        var $parentDiv = $canvas.parent();
        // select tab to draw
        $('a[href="#HandtekeningTab"]').click();
        //reset width and hight to parent div size
        canvas.width = $parentDiv.innerWidth();
        canvas.height = $parentDiv.innerHeight();
    
        //draw image if one existed
        if($element.val()){
            var myImage = new Image();
            myImage.src = $element.val();
            myImage.onload = function() {
                context.drawImage(myImage, 0, 0);
            };
        }
    
        // Handle canvas mouse events to enable adding drawing instructions
        $canvas.on('mousedown', function (e) {
            var mouseX = e.pageX - $(this).offset().left;
            var mouseY = e.pageY - $(this).offset().top;
        
            isPainting = true;
            $canvas.addClass('focus');
            new Click(mouseX, mouseY);
            redraw();
        });
        $canvas.on('mousemove', function (e) {
            if (isPainting) {
                var mouseX = e.pageX - $(this).offset().left;
                var mouseY = e.pageY - $(this).offset().top;
            
                new Click(mouseX, mouseY, true);
                redraw();
            }
        });
        $(document).on('mouseup', function (e) {
            isPainting = false;
            $canvas.removeClass('focus');
        });
    
        // Handle canvas touch events to 'redirect' to mouse event
        $canvas.on('touchstart', function (e) {
            var touch = e.originalEvent.touches[0];
            var mouseEvent = new MouseEvent('mousedown', {
                // Note: Setting clientX-Y also sets pageX-Y, setting pageX-Y is not allowed
                clientX: touch.pageX,
                clientY: touch.pageY
            });
            canvas.dispatchEvent(mouseEvent);
        });
        $canvas.on('touchmove', function (e) {
            var touch = e.originalEvent.touches[0];
            var mouseEvent = new MouseEvent('mousemove', {
                // Note: Setting clientX-Y also sets pageX-Y, setting pageX-Y is not allowed
                clientX: touch.pageX,
                clientY: touch.pageY
            });
            canvas.dispatchEvent(mouseEvent);
        });
        $canvas.on('touchend', function (e) {
            var mouseEvent = new MouseEvent('mouseup');
            canvas.dispatchEvent(mouseEvent);
        });
    
        // Prevent scrolling on the canvas for touchscreens
        $('body').on('touchstart touchmove touchend', function (e) {
            if (e.target === canvas)
                e.preventDefault();
        });
    
        // Clear the canvas
        $clear.on('click', function (e) {
            clicks = [];
            redraw();
        });
    
        // Extract canvas DataURL into hidden input
        $canvas.closest('form').on('submit', function (e) {
            var data = canvas.toDataURL();
            $element.val(data);
        });
    
    
        var clicks = [];
        var isPainting;
    
        // Keep track of canvas clicks/positions
        function Click(x, y, dragging) {
            this.x = x;
            this.y = y;
            this.dragging = dragging || false;
            clicks.push(this);
        }
    
        // The actual canvas drawing
        function redraw() {
            context.clearRect(0, 0, context.canvas.width, context.canvas.height);
        
            context.strokeStyle = '#000000';
            context.lineJoin = 'round';
            context.lineWidth = 5;
        
            for (var i = 0; i < clicks.length; i++) {
                context.beginPath();
                if (clicks[i].dragging && i)
                    context.moveTo(clicks[i - 1].x, clicks[i - 1].y);
                else
                    context.moveTo(clicks[i].x - 1, clicks[i].y);
            
                context.lineTo(clicks[i].x, clicks[i].y);
                context.closePath();
                context.stroke();
            }
        }
        $('a[href="#AlgemeneGegevensTab"]').click();
    };
    
    $(function () {
        setTimeout(DoDraw, 100);
    });
})(window, document, jQuery);