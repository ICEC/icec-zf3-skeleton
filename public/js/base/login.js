(function (window, document, $, undefined) {
    
     A2C.GetLoginForm = function () {
        $('#offCanvasRight').foundation('close');
        $.ajax({
            url: '/ajax/login',
            type: 'post',
            data: {id: 0},
            success: function (data) {
                $('#LoginPanel').html(data).foundation('open');
            }
        });
    };
    
    var TryAuthentication = function () {
        var $LoginSubmitLoader = $('.LoginSubmitLoader');
        $LoginSubmitLoader.show();
        $.ajax({
            url: '/ajax/authenticate',
            type: 'POST',
            data: $('*[data-form="postform"]').serialize(),
            success: function (data) {
                $('#LoginPanel').html(data);
                $LoginSubmitLoader.hide();
                if ($('*[data-form="postform"]').find('input[name="succes"]').val()) {
                    
                    $('.LoginFormSection').hide();
                    $('.LoginPanelActionButtonRow').hide();
                    $('.SuccesFullModal').show();
                    
                    //Show succes and redirect message
                    window.setTimeout(function() {
                        window.location.href = 'dashboard'
                    }, 2000);
                }else{
                    A2C.MarkFormErrors();
                }
            }
        });
    };
    
    var TryAuthenticationReset = function () {
        var $LoginSubmitLoader = $('.LoginSubmitLoader');
        $LoginSubmitLoader.show();
        $.ajax({
            url: '/ajax/authenticatereset',
            type: 'POST',
            data: $('*[data-form="postform"]').serialize(),
            success: function (data) {
                $('#LoginPanel').html(data);
                $LoginSubmitLoader.hide();
                var $form = $('*[data-form="postform"]');
                if ($form.find('input[name="succes"]').val()) {
                    $form.find('input[name="vergeten"][type="checkbox"]').prop('checked',false);
                    $('.HerstelEmailer').html($form.find('input[name="email"]').val());
                    $('.ResetSucces').show();
                }else{
                    A2C.MarkFormErrors();
                }
            }
        });
    };
    
    var CloseModal = function () {
        $('#LoginPanel').foundation('close');
    };
    
    var ToggleWachtWoordHerstel = function () {
        if ($('#LoginPanel').find('input[name="vergeten"]').is(':checked')) {
            ShowWachtWoordReset();
        } else {
            HideWachtWoordReset();
        }
    };
    
    var ShowWachtWoordReset = function () {
        $('#LoginPanel').find('input[name="password"]').prop('disabled',true);
        $('.DoActionButton').find('i').removeClass('success TryAuthentication')
            .addClass('alert TryAuthenticationReset')
            .html('Wachtwoord herstel');
    };
    
    var HideWachtWoordReset = function () {
        $('#LoginPanel').find('input[name="password"]').prop('disabled',false);
        $('.DoActionButton').find('i').removeClass('alert TryAuthenticationReset')
            .addClass('success TryAuthentication')
            .html('Inloggen');
    };
    
    $(function () {
        $(document)
            .on('click', '.GetAuthForm', A2C.GetLoginForm)
            .on('click', '.ModalCloseButton', CloseModal)
            .on('click','.TryAuthentication', TryAuthentication)
            .on('click','.TryAuthenticationReset', TryAuthenticationReset)
            .on('change', $('#LoginPanel').find('input[name="vergeten"]'), ToggleWachtWoordHerstel)
        ;
        // setTimeout(GetLoginForm(), 0);
    });
}(window, document, jQuery));