(function (window, document, $, undefined) {
    
    var GetResetLoginForm = function () {
        $('#offCanvasRight').foundation('close');
        $.ajax({
            url: '/ajax/getresetform',
            type: 'post',
            data: {token: $('input[name="url_token"]').val()},
            success: function (data) {
                $('#LoginPanel').html(data).foundation('open');
            }
        });
    };
    
    var DoAuthenticationReset = function () {
        var $LoginSubmitLoader = $('.LoginSubmitLoader');
        $LoginSubmitLoader.show();
        $.ajax({
            url: '/ajax/AuthenticationReset',
            type: 'POST',
            data: $('*[data-form="postform"]').serialize(),
            success: function (data) {
                $('#LoginPanel').html(data);
                $LoginSubmitLoader.hide();
                var $form = $('*[data-form="postform"]');
                if ($form.find('input[name="succes"]').val()) {
                    $form.find('input[name="vergeten"][type="checkbox"]').prop('checked',false);
                    $('.ResetSucces').show();
                    $('.LoginPanelActionButtonRow').hide();
                    $('.LoginFormSection').hide();
                    $('.SuccesFullModal').show();
                    window.setTimeout(function() {
                        window.location.href = '/login'
                    }, 2000);
                }else{
                    A2C.MarkFormErrors();
                }
            }
        });
    };
    
    var CloseModal = function () {
        $('#LoginPanel').foundation('close');
    };
    
    $(function () {
        $(document)
            .on('click', '.ModalCloseButton', CloseModal)
            .on('click','.DoAuthenticationReset', DoAuthenticationReset)
        ;
        setTimeout(GetResetLoginForm(), 0);
    });
}(window, document, jQuery));