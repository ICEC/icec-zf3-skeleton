(function (window, document, $, undefined) {
    "use strict";
    // Create the base A2C object
    var A2C = {};
    window.A2C = A2C;
    // Disable override of window.$
    $.noConflict();
    
    A2C.SubmitForm = function (e) {
        var $form = $('*[data-form="postform"]');
        $form.submit();
    };
    
    A2C.CloseModal = function () {
        $('#ModalMessage').foundation('close');
    };
    
    A2C.ShowCommonLoader = function () {
        $('#ProgressModalMessage').foundation('open');
    };
    A2C.UpdateCommonLoaderMessage = function (message) {
        $('.ProgressModalSpinner').toggle();
        $('#ProgressModalMessage').find('.LoaderMessage').html(message);
    };
    
    A2C.HideCommonLoader = function () {
        ('#ProgressModalMessage').foundation('close');
    };
    
    A2C.ExpandCloseButton = function () {
        $('.ModalCloseButton').parent()
            .removeClass('large-3 medium-3 small-3')
            .addClass('large-6 medium-6  small-6');
    };
    
    A2C.MarkFormErrors = function () {
        $('.form-error').each(function (e) {
            if ($(this).text().length > 0) {
                $(this).addClass('is-visible');
            }
        });
    };
    
    A2C.CloseXLModal = function () {
        $('#XLModalMessage').foundation('close');
    };
    
    A2C.DisableFormInputs = function () {
        var $form = $('*[data-form="postform"]');
        $form.find('input').prop('readonly',true);
        $form.find('select').attr('disabled',true);
    };
    
    var logout = function (e) {
        if(!confirm('Wilt u uitloggen ?')){
            e.preventDefault();
            return false;
        }
    };
    
    var toggleMenuButton = function () {
        $('#HomeMenuButton').toggleClass('open');
    };
    
    $(function () {
        $(document)
            //Close button for default(layout) modal
            .on('click', '.ModalCloseButton', A2C.CloseModal)
    
            .on('closed.zf.offcanvas',toggleMenuButton)
            .on('opened.zf.offcanvas',toggleMenuButton)
            .on('click','.LogOutButton',logout)
            .foundation()
        ;
        A2C.MarkFormErrors();
        $('.SubmitButton').on('click', A2C.SubmitForm);
        $('#offCanvasRight').show();
    });
}(window, document, jQuery));

