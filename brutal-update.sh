#!/bin/sh

cd vendor/a2cool/zf3-circlical-user
git branch
git fetch --all
git pull

cd ..
cd zf3base
git branch
git fetch --all
git pull

cd ..
cd ..
cd ..
git branch
git fetch --all
git pull


cd vendor/bin/
echo !! Doing some validation first !!
echo 
php doctrine-module orm:validate-schema

echo 
echo Validation done lets update
echo

php doctrine-module orm:schema-tool:update --force

echo

echo !! Rerun validation !!
echo
php doctrine-module orm:validate-schema

echo


php doctrine-module orm:clear-cache:metadata
php doctrine-module orm:clear-cache:query
php doctrine-module orm:clear-cache:result
php doctrine-module orm:generate-proxies

echo !! All Done !!
timeout 5 sleep 6

