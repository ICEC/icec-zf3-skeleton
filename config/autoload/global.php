<?php

use Application\Controller\Plugin\AuthenticationPlugin;
use Application\View\Helper\Factory\FoundationFormLabelHelperFactory;
use CirclicalUser\Factory\Service\AuthenticationServiceFactory;
use CirclicalUser\Service\AuthenticationService;
use DoctrineORMModule\Yuml\YumlController;
use Zend\Mvc\Plugin\Prg\PostRedirectGet;
use Zend\ServiceManager\Factory\InvokableFactory;

return array(
    'doctrine' => [
        'authentication' => [
            'orm_default' => [
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'Application\Entity\User',
                'identityProperty' => 'username',
                'credentialProperty' => 'password',
                'credentialCallable' => 'Application\Entity\User::checkPassword'
            ],
        ],
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => __DIR__ . '/../../data/migrations',
                'namespace' => 'A2C\Migrations',
                'table' => 'migrations',
            ],
        ],
        'configuration' => array(
            'orm_default' => array(
                'string_functions' => array(
                    'group_concat' => DoctrineExtensions\Query\Mysql\GroupConcat::class
                ),
                'filters' => array(
                    'soft-deleteable' => Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter::class,
                ),
                'metadata_cache'    => 'zend.static.local',
                'query_cache'       => 'zend.static.local',
                'result_cache'      => 'zend.static.local',
                'generate_proxies'  => false,
            )
        ),
        'eventmanager' => array(
            'orm_default' => array(
                'subscribers' => array(
                    \Gedmo\SoftDeleteable\SoftDeleteableListener::class
                ),
            ),
        ),
    ],
    'circlical' => [
        'user' => [
            'doctrine' => [
                'entity' => \Application\Entity\User::class,
            ],
            'auth' => [
                'crypto_key' => 'sfZGFm1rCc7TgPr9aly3WOtAfbEOb/VafB8L3velkd0=',
                'transient' => false,
                'password_reset_tokens' => [
                    'enabled' => true,
                ]
            ],
            'deny_strategy' => [
                'class' => \CirclicalUser\Strategy\RedirectStrategy::class,
                'options' => [
                    'controller' => \Application\Controller\IndexController::class,
                    'action' => 'login',
                ],
            ],
            
            'providers' => [
                //                'auth' => \CirclicalUser\Mapper\AuthenticationMapper::class,                
                //                'user' => \CirclicalUser\Mapper\UserMapper::class,                
                //                'role' => \CirclicalUser\Mapper\RoleMapper::class,
                
                'rules' => [
                    //                    'group' => \CirclicalUser\Mapper\GroupPermissionMapper::class,                    
                    //                    'user' => \CirclicalUser\Mapper\UserPermissionMapper::class,                
                ],
            ],

            'guards' => [
                'ModuleName' => [
                    "controllers" => [
                        YumlController::class => [
                            'default' => [], // anyone can access
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_helpers' => array(
        'invokables' => [
            'translate' => \Zend\I18n\View\Helper\Translate::class
        ],
        'factories' => array(
            'FoundationFormLabelHelper' => FoundationFormLabelHelperFactory::class
        ),
    ),
    'service_manager' => [
        'factories' => [
            AuthenticationService::class  => AuthenticationServiceFactory::class,
            'translator' => \Zend\I18n\Translator\TranslatorServiceFactory::class,
            'doctrine.cache.zend.static.local' => function ($services) {
                return new \DoctrineModule\Cache\ZendStorageCache($services->get('cache.static.local'));
            },
        ],
    ],
    'controller_plugins' => [
        'aliases' => [
            'prg'             => PostRedirectGet::class,
            'PostRedirectGet' => PostRedirectGet::class,
            'postRedirectGet' => PostRedirectGet::class,
            'postredirectget' => PostRedirectGet::class,
            'Zend\Mvc\Controller\Plugin\PostRedirectGet' => PostRedirectGet::class,
            
            'AuthPlugin' => AuthenticationPlugin::class,
        ],
        
        'factories' => [
            PostRedirectGet::class => InvokableFactory::class,
            AuthenticationPlugin::class => \Application\Controller\Plugin\Factory\AuthenticationPluginFactory::class,
        ],
    ],
);