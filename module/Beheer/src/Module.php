<?php
namespace Beheer;

class Module
{
    const VERSION = '3.0.3-dev';
    
    public function getConfig() {
        $config = array();
        foreach (glob(__DIR__ . '/../config/*.config.php') as $filename)
            $config = array_merge($config, include $filename);

        return $config;
    }
}
