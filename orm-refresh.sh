#!/bin/sh
cd vendor/bin/

php doctrine-module orm:clear-cache:metadata
php doctrine-module orm:clear-cache:query
php doctrine-module orm:clear-cache:result
php doctrine-module orm:generate-proxies

echo !! All Done !!
timeout 5 sleep 6

